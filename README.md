# quran.hadaf.id

## User Stories
- User can find ayat(h) based on keyword similarity
- User can save n label ayat(h) selection
- User can play audio of ayah selection repeatedly for memorizing
- User can find ayah based on related topic 

## Resources

- [rioastamal / quran-json](https://github.com/rioastamal/quran-json)
- [semarketir/quranjson](https://github.com/semarketir/quranjson)
- [zusmani/the-holy-quran](https://www.kaggle.com/zusmani/the-holy-quran)
